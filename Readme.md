# Quaranixit

A client and server web implementation of Dixit the game

## Getting Started

You should install nodejs on your machine. [Here](https://nodejs.org/it/)

Then install Yarn globaly, run `npm i -g yarn`.

Then in the root of the project run `yarn` and thar should install all the dependency.

Then run `yarn bootstrap`.

In order to start a dev environment run `yarn start:dev`, the client and the server should start.

## Todo

- [x] Finish game engine implementation (with tests)
- [x] Define socket.io actions
- [x] Create an enter player page and a new game page
- [x] Create a board and hand component
- [x] Wired up all the game engine action with the buttons ;)
- [ ] Create a docker image
- [ ] Use redis as db for game state
