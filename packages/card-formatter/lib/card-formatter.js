#!/usr/bin/env node

const fs = require("fs");
const { join, resolve } = require("path");
const { promisify } = require("util");

const readdir = promisify(fs.readdir);
const copyFile = promisify(fs.copyFile);
const mkdir = promisify(fs.mkdir);
const exists = promisify(fs.exists);
const rm = promisify(fs.rmdir);

const startDir = resolve(join(__dirname, "..", "start_folder"));
const destDir = resolve(join(__dirname, "..", "dest_folder"));

async function main() {
  if (await exists(destDir)) {
    await rm(destDir, { recursive: true });
  }
  await mkdir(destDir);
  const images = await readdir(startDir);
  console.log(`Loading ${images.length} images`);
  const copyes = images.map((img, index) =>
    copyFile(join(startDir, img), join(destDir, `card_${index}.jpg`))
  );
  await Promise.all(copyes);
}

main().then(() => console.log("DONE"));
