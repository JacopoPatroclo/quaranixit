import React, { createContext } from "react";
import { useGameState } from "./useGameState";
import { App } from "../App";

export const GameStateContext = createContext();

const mockGameState = (gameState) => {
  gameState.you = "o3WfvAz-WvrwpS6wAAAS";
  gameState.gameState = {
    id: "3F2C",
    players: [
      {
        id: "o3WfvAz-WvrwpS6wAAAS",
        name: "asdasd",
        points: 4,
        hand: [
          {
            id: "card_00077",
            link: "/cards/card_00077.jpg",
          },
          {
            id: "card_00019",
            link: "/cards/card_00019.jpg",
          },
          {
            id: "card_00036",
            link: "/cards/card_00036.jpg",
          },
          {
            id: "card_00099",
            link: "/cards/card_00099.jpg",
          },
        ],
      },
      {
        id: "xJuJTT008H8WcXdmAAAT",
        name: "asdasdasd",
        points: 0,
        hand: [
          {
            id: "card_00071",
            link: "/cards/card_00071.jpg",
          },
          {
            id: "card_00049",
            link: "/cards/card_00049.jpg",
          },
          {
            id: "card_00072",
            link: "/cards/card_00072.jpg",
          },
          {
            id: "card_00076",
            link: "/cards/card_00076.jpg",
          },
        ],
      },
      {
        id: "u66G6E3Xjf32LyjxAAAU",
        name: "askdmaksdmask",
        points: 0,
        hand: [
          {
            id: "card_00050",
            link: "/cards/card_00050.jpg",
          },
          {
            id: "card_00057",
            link: "/cards/card_00057.jpg",
          },
          {
            id: "card_00088",
            link: "/cards/card_00088.jpg",
          },
          {
            id: "card_00045",
            link: "/cards/card_00045.jpg",
          },
        ],
      },
    ],
    board: {
      visible: true,
      cards: [
        {
          card: {
            id: "card_00073",
            link: "/cards/card_00073.jpg",
          },
          pl_name: "askdmaksdmask",
        },
        {
          card: {
            id: "card_00058",
            link: "/cards/card_00058.jpg",
          },
          pl_name: "asdasdasd",
        },
        {
          card: {
            id: "card_00018",
            link: "/cards/card_00018.jpg",
          },
          pl_name: "asdasd",
        },
        {
          card: {
            id: "card_00058",
            link: "/cards/card_00058.jpg",
          },
          pl_name: "asdasdasd",
        },
        {
          card: {
            id: "card_00018",
            link: "/cards/card_00018.jpg",
          },
          pl_name: "asdasd",
        },
        {
          card: {
            id: "card_00058",
            link: "/cards/card_00058.jpg",
          },
          pl_name: "asdasdasd",
        },
        {
          card: {
            id: "card_00018",
            link: "/cards/card_00018.jpg",
          },
          pl_name: "asdasd",
        },
      ],
    },
    guesses: {
      asdasdasd: {
        id: "card_00018",
        link: "/cards/card_00018.jpg",
      },
    },
    narrator: "asdasd",
    aviablePlayers: 12,
    state: "PLAYING",
  };
  gameState.gameState.winner = {
    id: "u66G6E3Xjf32LyjxAAAU",
    name: "askdmaksdmask",
    points: 20,
  };
  return gameState;
};

export const Index = () => {
  let gameState = useGameState();

  // Uncomment this if you want to test the app in some fixed state
  // gameState = mockGameState(gameState);

  return (
    <GameStateContext.Provider value={gameState}>
      <App />
    </GameStateContext.Provider>
  );
};
