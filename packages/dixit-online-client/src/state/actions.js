import {
  NEW_GAME,
  JOIN_GAME,
  LEAVE_GAME,
  START_PLAING,
  PLAY_CARD,
  VOTE_CARD,
  NEXT_TURN,
  GET_GAME_STATE,
} from "actions";

export const makeActions = (socket) => ({
  newGame: ({ name }) => (event) => {
    socket.emit(NEW_GAME, { name });
  },

  joinGame: ({ gameId, name }) => (event) => {
    socket.emit(JOIN_GAME, { gameId, name });
  },

  leaveGame: ({ gameId, playerId }) => (event) => {
    socket.emit(LEAVE_GAME, { gameId, playerId });
  },

  startPlay: ({ gameId }) => (event) => {
    socket.emit(START_PLAING, { gameId });
  },

  playCard: ({ gameId, playerName, cardId }) => (event) => {
    socket.emit(PLAY_CARD, { gameId, playerName, cardId });
  },

  guessCard: ({ gameId, playerName, cardId }) => (event) => {
    socket.emit(VOTE_CARD, { gameId, playerName, cardId });
  },

  nextTurn: ({ gameId }) => () => {
    socket.emit(NEXT_TURN, { gameId });
  },

  getState: ({ gameId }) => () => {
    socket.emit(GET_GAME_STATE, { gameId });
  },
});
