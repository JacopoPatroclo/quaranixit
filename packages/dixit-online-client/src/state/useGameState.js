import { useState, useEffect } from "react";
import { UPDATE_GAME_STATE, GAME_ERROR, IS_THE_CURRENT_PLAYER } from "actions";
import { makeActions } from "./actions";
import io from "socket.io-client";

const socket = io({ path: "/ws" });

export const useGameState = () => {
  const [gameState, setState] = useState(null);
  const [you, setYou] = useState(null);
  const [errors, setError] = useState([]);
  const [playerName, setPlayerName] = useState(null);
  const actions = makeActions(socket);

  useEffect(() => {
    const reconnection = socket.on("reconnect", () => {
      setError(errors.filter((error) => error.id !== "offline_error"));
    });

    const reconnectionAttempts = socket.on("reconnect_attempt", () => {
      console.log(
        `Reconnect as ${playerName} in game ${
          gameState ? gameState.id : "nogame"
        }`
      );
      if (gameState) {
        socket.io.opts.query = {
          gameId: gameState ? gameState.id : null,
          playerName,
        };
      }
    });

    const reconnectionError = socket.on("reconnect_failed", () => {
      setError([
        ...errors,
        {
          id: "reconnect_error",
          message: "Unable to reconnecto to the server",
        },
      ]);
    });

    const disconnection = socket.on("disconnect", () => {
      setError([
        ...errors,
        {
          id: "offline_error",
          message: "You are now offline",
        },
      ]);
    });

    const gameStateListener = socket.on(UPDATE_GAME_STATE, (newGameState) => {
      console.log("UPDATE_STATE", newGameState);
      setState(newGameState);
    });

    const errorStateListener = socket.on(GAME_ERROR, (error) => {
      setError([...errors, error]);
    });

    const currentPlayer = socket.on(IS_THE_CURRENT_PLAYER, (your_id) => {
      if (gameState) {
        const currentUser = gameState.players.find(
          (player) => player.id === your_id
        );
        if (currentUser && currentUser.name !== playerName) {
          setPlayerName(currentUser.name);
        }
      }
      setYou(your_id);
    });

    return () => {
      gameStateListener.removeEventListener(UPDATE_GAME_STATE);
      errorStateListener.removeEventListener(GAME_ERROR);
      currentPlayer.removeEventListener(IS_THE_CURRENT_PLAYER);
      reconnection.removeEventListener("reconnect");
      reconnectionError.removeEventListener("reconnect_failed");
      disconnection.removeEventListener("disconnect");
      reconnectionAttempts.removeEventListener("reconnect_attempt");
    };
  });

  const cancelError = (id) => {
    return (e) => {
      e.preventDefault();
      setError(errors.filter((error) => error.id !== id));
    };
  };

  return {
    gameState,
    errors,
    cancelError,
    you,
    ...actions,
  };
};
