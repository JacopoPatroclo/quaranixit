import React, { useContext } from "react";
import cx from "classnames";

import PlayerWaitingStyle from "./PlayerWaiting.module.scss";
import { GameStateContext } from "../state/Index";

export const PlayerWaiting = () => {
  const { gameState, you, leaveGame, startPlay } = useContext(GameStateContext);

  let numberOfMissingSpots =
    gameState.aviablePlayers - gameState.players.length;
  let players = [...gameState.players];

  if (numberOfMissingSpots > 0) {
    players.push({ missingSpot: true });
    while ((numberOfMissingSpots -= 1)) {
      players.push({ missingSpot: true });
    }
  }

  const playersMatrix = [];

  while (players.length > 0) {
    playersMatrix.push(
      players.splice(0, players.length > 4 ? 4 : players.length)
    );
  }

  return (
    <div className="container">
      <div className="container">
        <a
          style={{ margin: "auto" }}
          className="button is-large is-primary"
          onClick={startPlay({ gameId: gameState.id })}
        >
          All in let's play!!
        </a>
      </div>
      <div className="container" style={{ paddingTop: 50 }}>
        {playersMatrix.map((row, index) => (
          <div key={index} className="columns">
            {row.map((player, index) =>
              player.missingSpot ? (
                <div key={index} className="column">
                  <div className={PlayerWaitingStyle.cardContainer}>
                    <span className="icon is-large">
                      <i className="fas fa-3x fa-spinner fa-pulse" />
                    </span>
                    <p className="is-size-3">Waiting...</p>
                  </div>
                </div>
              ) : (
                <div key={player.id} className="column">
                  <div
                    className={cx(PlayerWaitingStyle.cardContainer, {
                      [PlayerWaitingStyle.isyou]: player.id === you,
                    })}
                  >
                    <img
                      src="https://svgsilh.com/svg/1456875.svg"
                      width={100}
                    />
                    <p className="is-size-3">{player.name}</p>
                    {player.id === you ? (
                      <a
                        className="button is-danger"
                        onClick={leaveGame({
                          gameId: gameState.id,
                          playerId: you,
                        })}
                      >
                        Leave
                      </a>
                    ) : null}
                  </div>
                </div>
              )
            )}
          </div>
        ))}
      </div>
    </div>
  );
};
