import React, { useContext } from "react";
import { GameStateContext } from "../state/Index";

import "./Winning.scss";

export const Winning = () => {
  const { gameState, you, leaveGame } = useContext(GameStateContext);
  const winner = gameState.winner;
  const isYou = winner ? winner.id === you : false;

  return (
    <div
      className="pyro is-flex"
      style={{
        height: "80vh",
      }}
    >
      <div className="before"></div>
      <div style={{ margin: "auto", display: "flex", flexDirection: "column" }}>
        <h1 className="title is-size-1">
          {isYou ? "You are" : `${winner.name} is`} the winner!!
        </h1>
        <a
          style={{ margin: "auto" }}
          onClick={leaveGame({ gameId: gameState.id, playerId: you })}
          className="button is-muted"
        >
          Leave
        </a>
      </div>
      <div className="after"></div>
    </div>
  );
};
