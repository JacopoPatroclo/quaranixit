import React, { useState, useContext } from "react";
import { GameStateContext } from "../state/Index";

export const Landing = () => {
  const [gameId, setGameId] = useState("");
  const [name, setUserName] = useState("");

  const { newGame, joinGame } = useContext(GameStateContext);

  const newGameClick = (e) => {
    newGame({ name })(e);
  };

  const joinGameClick = (e) => {
    joinGame({ gameId, name })(e);
  };

  return (
    <div style={{ paddingTop: "10vh" }}>
      <div className="columns">
        <div className="column is-full">
          <label className="label is-size-1">Username</label>
          <div className="field-body">
            <div className="field">
              <p className="control">
                <input
                  className="input is-large"
                  type="text"
                  placeholder="Fluffikins"
                  value={name}
                  onChange={(e) => setUserName(e.target.value)}
                />
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column is-half">
          <div className="field">
            <label className="label is-size-1">Join a Game</label>
            <div className="field-body">
              <div className="field">
                <p className="control">
                  <input
                    className="input is-large"
                    type="text"
                    placeholder="ex. e43d"
                    value={gameId}
                    onChange={(e) => setGameId(e.target.value.toUpperCase())}
                  />
                </p>
              </div>
            </div>
          </div>
          <a className="button is-primary is-large" onClick={joinGameClick}>
            Join
          </a>
        </div>
        <div className="column is-half">
          <div className="field">
            <label className="label is-size-1">Start a new game</label>
          </div>
          <a className="button is-info is-large" onClick={newGameClick}>
            Go
          </a>
        </div>
      </div>
    </div>
  );
};
