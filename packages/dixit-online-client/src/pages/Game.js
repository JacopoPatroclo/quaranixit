import React, { useContext } from "react";
import { GameStateContext } from "../state/Index";
import { Hand } from "../components/Hand/Hand";
import { ScoreBoard } from "../components/Scoreboard/Scoreboard";
import { Board } from "../components/Board/Board";

import GameStyle from "./Game.module.scss";

export const Game = () => {
  return (
    <div className="container is-widescreen is-flex">
      <div className={GameStyle.twothird}>
        <div>
          <Board />
        </div>
        <div>
          <Hand />
        </div>
      </div>
      <div className={GameStyle.onefirth}>
        <ScoreBoard />
      </div>
    </div>
  );
};
