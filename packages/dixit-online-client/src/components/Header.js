import React, { useContext } from "react";
import { GameStateContext } from "../state/Index";

export const Header = () => {
  const { gameState } = useContext(GameStateContext);

  return (
    <div className="container" style={{ paddingBottom: 10 }}>
      <h1 className="is-size-1 has-text-primary">
        {gameState ? gameState.id : "No game code aviable"}
      </h1>
      <p className="subtitle">Use this code to enter in this game</p>
    </div>
  );
};
