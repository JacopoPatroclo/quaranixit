import React, { useContext, useState, useEffect } from "react";
import { GameStateContext } from "../../state/Index";
import { SHOWING } from "gamestates";
import ScoreBoardStyle from "./Scoreboard.module.scss";

export const AnimatedPoints = ({ points }) => {
  const [animatedPoint, setPoint] = useState(0);
  const [isIncreMentig, setIncrementing] = useState(false);
  useEffect(() => {
    if (animatedPoint < points) {
      setIncrementing(true);
      setTimeout(() => {
        setPoint(animatedPoint + 1);
      }, 500);
    } else {
      setIncrementing(false);
    }
    return () => {};
  });
  return (
    <span
      style={{ color: isIncreMentig ? "green" : "black" }}
      className={ScoreBoardStyle.animatedPoint}
    >
      {animatedPoint}
    </span>
  );
};

export const ScoreBoard = () => {
  const { gameState, you, nextTurn } = useContext(GameStateContext);

  const score = gameState.players.map((ply) => ({
    pl_id: ply.id,
    pl_name: ply.name,
    points: ply.points,
  }));
  const youPlayer = gameState.players.find(({ id }) => you === id);

  const narrator = gameState.narrator;
  const youAreTheNarrator = you
    ? youPlayer
      ? youPlayer.name === narrator
      : false
    : false;

  return (
    <div
      className="is-flex is-size-4"
      style={{ flexDirection: "column", minWidth: 300, paddingLeft: 24 }}
    >
      <h2 className="is-size-2">Score Board</h2>
      {score.map(({ pl_name, points, pl_id }) => (
        <p
          className={ScoreBoardStyle.listItem}
          isnarrator={`${narrator === pl_name}`}
          isyou={`${you === pl_id}`}
          key={pl_id}
        >
          {pl_name}: <AnimatedPoints points={points} />
        </p>
      ))}
      <h2 className="is-size-2">Games actions</h2>
      {gameState.state === SHOWING && youAreTheNarrator ? (
        <a
          className="button is-primary"
          onClick={nextTurn({ gameId: gameState.id })}
        >
          Next turn
        </a>
      ) : null}
    </div>
  );
};
