import React, { useContext } from "react";
import { GameStateContext } from "../../state/Index";
import { SHOWING } from "gamestates";
import cx from "classnames";

import BoardStyle from "./Board.module.scss";

export const CardsAndSpots = ({
  spots,
  visible,
  playerName,
  guessCard,
  gameId,
  chosenCard,
  isEndTunr,
  guesses,
  narrator,
}) => {
  const isTheRightOne = (name) => name === narrator;
  return (
    <div className="is-flex" style={{ flexWrap: "wrap" }}>
      {spots.map((spot, index) => {
        return (
          <div key={index} className={BoardStyle.cardWrapper}>
            {isEndTunr ? (
              <div
                className={BoardStyle.infoCard}
                style={{
                  backgroundColor: isTheRightOne(spot.pl_name)
                    ? // green
                      "rgba(0, 230, 64, 0.8)"
                    : //black
                      "rgba(0, 0, 0, 0.7)",
                }}
              >
                <p className="is-medium" style={{ wordBreak: "break-word" }}>
                  From{" "}
                  <strong style={{ color: "white" }}>
                    {spot.pl_name || "No One"}
                  </strong>
                </p>
                <p>Voted by:</p>
                {guesses
                  .filter(({ id }) => (spot.card ? spot.card.id === id : false))
                  .map(({ pl_name }, index) => (
                    <p style={{ paddingLeft: 10 }} key={`${index}-people`}>
                      - {pl_name}
                    </p>
                  ))}
              </div>
            ) : null}
            <div
              className={cx(BoardStyle.card, {
                [BoardStyle.isMissing]: !spot.card,
              })}
              isvisible={`${visible && !!spot.card}`}
              ischoosen={`${
                !!chosenCard &&
                chosenCard.id === (spot.card ? spot.card.id : null)
              }`}
              onClick={(e) => {
                if (visible && !!spot.card) {
                  guessCard({ gameId, playerName, cardId: spot.card.id })(e);
                }
              }}
            >
              {spot.card ? (
                <>
                  <img
                    className={BoardStyle.img}
                    src={visible ? spot.card.link : "/dixit_back.jpg"}
                  />
                  <p
                    style={
                      playerName === spot.pl_name
                        ? { color: "black", backgroundColor: "yellow" }
                        : {}
                    }
                    className={BoardStyle.counterIcon}
                  >
                    {index + 1}
                  </p>
                </>
              ) : null}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export const Board = () => {
  const { gameState, you, guessCard } = useContext(GameStateContext);

  const { board, players, id } = gameState;
  const currentPlayer = players.find((ply) => ply.id === you);
  const chosenCard =
    gameState.guesses[currentPlayer ? currentPlayer.name : "null"];

  const isEndTunr = gameState.state === SHOWING;

  const guesses = Object.keys(gameState.guesses).map((pl_name) => ({
    pl_name,
    ...gameState.guesses[pl_name],
  }));

  return (
    <div className="is-flex" style={{ paddingTop: 20 }}>
      <CardsAndSpots
        playerName={currentPlayer ? currentPlayer.name : "null"}
        spots={board.cards}
        visible={board.visible}
        guessCard={guessCard}
        gameId={id}
        chosenCard={chosenCard}
        isEndTunr={isEndTunr}
        guesses={guesses}
        narrator={gameState.narrator}
      />
    </div>
  );
};
