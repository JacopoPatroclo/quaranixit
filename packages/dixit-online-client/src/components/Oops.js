import React from "react";

export const Oops = ({ gameId }) => (
  <h1>
    Sembra che questa sessione sia scaduta, ricarica la pagina e rinetra in
    partita usando il tuo nome e questo codice {gameId}
  </h1>
);
