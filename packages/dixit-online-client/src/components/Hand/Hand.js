import React, { useContext, useState } from "react";
import { GameStateContext } from "../../state/Index";
import { Oops } from "../Oops";

import HandStyle from "./Hand.module.scss";

export const CardAnimated = ({ id, link, playerName, gameId }) => {
  const [played, setCardAsPlayed] = useState(false);
  const { playCard } = useContext(GameStateContext);

  return (
    <div
      key={Math.random()}
      className={HandStyle.card}
      isplayed={`${played}`}
      onAnimationEnd={(e) => {
        playCard({ gameId, playerName, cardId: id })(e);
        setCardAsPlayed(false);
      }}
    >
      <img
        onClick={() => setCardAsPlayed(true)}
        style={{
          height: 360,
        }}
        src={link}
      />
    </div>
  );
};

export const Hand = () => {
  const { gameState, you } = useContext(GameStateContext);
  const [handHidden, hideHand] = useState(false);

  const activePlayer = gameState.players.find((ply) => ply.id === you);

  return (
    <div className="is-flex" style={{ flexDirection: "column" }}>
      <div className="is-flex">
        {!!activePlayer && !handHidden ? (
          activePlayer.hand.map((card) => (
            <CardAnimated
              key={card.id}
              {...card}
              playerName={activePlayer.name}
              gameId={gameState.id}
            />
          ))
        ) : handHidden ? null : (
          <Oops gameId={gameState.id} />
        )}
      </div>
      <a
        className="button is-primary is-light is-small"
        onClick={() => hideHand(!handHidden)}
      >
        {handHidden ? "Show hand" : "Hide hand"}
      </a>
    </div>
  );
};
