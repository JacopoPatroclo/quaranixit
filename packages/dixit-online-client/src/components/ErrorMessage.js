import React, { useContext } from "react";
import ErrorMessageStyle from "./ErrorMessage.module.scss";
import { GameStateContext } from "../state/Index";

export const ErrorMessage = () => {
  const { errors, cancelError } = useContext(GameStateContext);

  return (
    <div className={ErrorMessageStyle.container}>
      {errors.map((error) => (
        <article key={error.id} className="message is-danger">
          <div className="message-header">
            <p>Danger, Will Robinson</p>
            <button
              className="delete"
              aria-label="delete"
              onClick={cancelError(error.id)}
            ></button>
          </div>
          <div className="message-body">{error.message}</div>
        </article>
      ))}
    </div>
  );
};
