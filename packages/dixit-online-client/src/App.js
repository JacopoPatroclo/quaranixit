import React, { useContext } from "react";
import { PENDING, PLAYING, GUESSING, SHOWING, WINNING } from "gamestates";
import { Landing } from "./pages/Landing";
import { ErrorMessage } from "./components/ErrorMessage";
import { PlayerWaiting } from "./pages/PlayerWaiting";
import { Game } from "./pages/Game";
import { GameStateContext } from "./state/Index";
import { Header } from "./components/Header";
import { Winning } from "./pages/Winning";

const Router = (state) => {
  if (state === PENDING) {
    return (
      <>
        <Header />
        <PlayerWaiting />
      </>
    );
  } else if (state === PLAYING || state === GUESSING || state === SHOWING) {
    return (
      <>
        <Header />
        <Game />
      </>
    );
  } else if (state === WINNING) {
    return <Winning />;
  } else {
    return null;
  }
};

export function App() {
  const { gameState } = useContext(GameStateContext);
  return (
    <div
      className="container is-widescreen"
      style={{ minHeight: "100vh", padding: 10 }}
    >
      {gameState ? Router(gameState.state) : <Landing></Landing>}
      <ErrorMessage />
    </div>
  );
}
