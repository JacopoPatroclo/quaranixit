require("dotenv").config();
const express = require("express");
const { setup } = require("./src");

const app = express();

const port = process.env.PORT || 5000;

setup(app).listen(port, () => {
  console.log(`Server started on ${port}`);
});
