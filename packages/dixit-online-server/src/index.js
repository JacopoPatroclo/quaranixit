const { createServer } = require("http");
const ws = require("./ws");
const { join } = require("path");
const express = require("express");

module.exports = {
  setup: (app) => {
    app.use(require("morgan")("common"));
    if (process.env.NODE_ENV === "prod") {
      app.use(express.static(join(__dirname, "..", "public")));
    }
    const http = createServer(app);
    ws.attach(http);
    return http;
  },
};
