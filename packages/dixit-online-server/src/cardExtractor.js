const { readdir } = require("fs");
const { resolve, parse } = require("path");
const { makeCardFromPublic } = require("./game/factories");

module.exports = (dir) =>
  new Promise((res, rej) => {
    readdir(resolve(dir), (err, files) => {
      if (err) {
        rej(err);
      }
      res(files.map((file) => makeCardFromPublic(parse(file).name)));
    });
  });
