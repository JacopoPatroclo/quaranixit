const { expect } = require("chai");
const { makePlayer, makeCardFromPublic } = require("./factories");

describe("Enforce structural consistency", () => {
  it("Should create a correct user", () => {
    const pl = makePlayer("Gino");
    expect(pl.name).eq("Gino");
    expect(typeof pl.id).eq("string");
  });

  it("Should create a correct card with the public uri", () => {
    const card = makeCardFromPublic("SomeCardName");
    expect(card.link).eq("/cards/SomeCardName.jpg");
    expect(typeof card.id).eq("string");
  });
});
