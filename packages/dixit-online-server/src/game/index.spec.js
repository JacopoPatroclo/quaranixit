const { expect } = require("chai");
const { TooMuchPlayersError } = require("./errors");
const makeDeck = require("./deck");

const makeGameEngine = require("./index");

const mockDeck = (draw = (a) => a, get = (a) => a, discard = (a) => a) => ({
  draw,
  get,
  discard,
});

const makeArray = (long, of) => {
  const l = [];
  l.push(of);
  while ((long -= 1)) {
    l.push(of);
  }
  return l;
};

describe("Test the game engine", () => {
  it("Should initiate a room", () => {
    const engine = makeGameEngine(3, mockDeck());
    expect(engine.getState().id).to.be.not.null;
    expect(typeof engine.getState().id).to.eq("string");
    expect(engine.getState().players.length).to.be.eq(0);
  });

  it("Should initiate a room with 3 spots", () => {
    const engine = makeGameEngine(3, mockDeck());
    expect(engine.getState().players.length).to.be.eq(0);
    engine.addPlayer({ name: "Ollo" });
    engine.addPlayer({ name: "Zambo" });
    engine.addPlayer({ name: "Martin" });
    expect(engine.addPlayer.bind(null, { name: "NoOne" })).to.throw(
      TooMuchPlayersError
    );
  });

  it("Should distribuite cards to the players", () => {
    const randomCard = {
      id: "randomcard",
      link: "/random.jpg",
    };
    const engine = makeGameEngine(
      3,
      mockDeck(
        (times) => (times === 1 ? randomCard : makeArray(times, randomCard)),
        () => randomCard
      )
    );
    engine.addPlayer({ name: "Ollo" });
    engine.addPlayer({ name: "Zambo" });
    engine.addPlayer({ name: "Martin" });
    engine.startGame();
    expect(engine.getState().players[0].hand.length).to.be.eq(5);
    expect(engine.getState().state).to.be.eq(engine.GAME_STATES.PLAYING);
  });

  it("Should play a card", () => {
    const deck = makeDeck([
      { id: "card_00029", link: "/cards/card_00029.jpg" },
      { id: "card_00061", link: "/cards/card_00061.jpg" },
      { id: "card_00094", link: "/cards/card_00094.jpg" },
      { id: "card_00038", link: "/cards/card_00038.jpg" },
      { id: "card_00052", link: "/cards/card_00052.jpg" },
      { id: "card_00036", link: "/cards/card_00036.jpg" },
      { id: "card_00035", link: "/cards/card_00035.jpg" },
      { id: "card_00099", link: "/cards/card_00099.jpg" },
      { id: "card_00041", link: "/cards/card_00041.jpg" },
      { id: "card_00004", link: "/cards/card_00004.jpg" },
      { id: "card_00017", link: "/cards/card_00017.jpg" },
      { id: "card_00048", link: "/cards/card_00048.jpg" },
      { id: "card_00053", link: "/cards/card_00053.jpg" },
      { id: "card_00057", link: "/cards/card_00053.jpg" },
      { id: "card_0005635", link: "/cards/card_00053.jpg" },
      { id: "card_00053455", link: "/cards/card_00053.jpg" },
    ]);
    const engine = makeGameEngine(3, deck);
    expect(engine.getState().players.length).to.be.eq(0);
    engine.addPlayer({ name: "Ollo", id: "ollo" });
    engine.addPlayer({ name: "Zambo", id: "zambo" });
    engine.addPlayer({ name: "Martin", id: "martin" });
    engine.startGame();
    const cardid = engine.getState().players.find((pl) => pl.id === "ollo")
      .hand[2].id;
    engine.playerPlayCard("Ollo", cardid);
    expect(
      engine
        .getState()
        .board.cards.find((boardSpot) => boardSpot.pl_name === "Ollo").card.id
    ).to.be.eq(cardid);
  });

  it("Should assign some points", () => {
    const deck = makeDeck([
      { id: "card_00029", link: "/cards/card_00029.jpg" },
      { id: "card_00061", link: "/cards/card_00061.jpg" },
      { id: "card_00094", link: "/cards/card_00094.jpg" },
      { id: "card_00038", link: "/cards/card_00038.jpg" },
      { id: "card_00052", link: "/cards/card_00052.jpg" },
      { id: "card_00036", link: "/cards/card_00036.jpg" },
      { id: "card_00035", link: "/cards/card_00035.jpg" },
      { id: "card_00099", link: "/cards/card_00099.jpg" },
      { id: "card_00041", link: "/cards/card_00041.jpg" },
      { id: "card_00004", link: "/cards/card_00004.jpg" },
      { id: "card_00017", link: "/cards/card_00017.jpg" },
      { id: "card_00048", link: "/cards/card_00048.jpg" },
      { id: "card_00053", link: "/cards/card_00053.jpg" },
      { id: "card_00057", link: "/cards/card_00053.jpg" },
      { id: "card_0005635", link: "/cards/card_00053.jpg" },
      { id: "card_00053455", link: "/cards/card_00053.jpg" },
    ]);
    const engine = makeGameEngine(3, deck);

    expect(engine.getState().players.length).to.be.eq(0);

    engine.addPlayer({ name: "Ollo", id: "ollo" });
    engine.addPlayer({ name: "Zambo", id: "zambo" });
    engine.addPlayer({ name: "Martin", id: "martin" });
    engine.startGame();

    const cardidOllo = engine.getState().players.find((pl) => pl.id === "ollo")
      .hand[2].id;
    const cardidZambo = engine
      .getState()
      .players.find((pl) => pl.id === "zambo").hand[2].id;
    const cardidMartin = engine
      .getState()
      .players.find((pl) => pl.id === "martin").hand[2].id;

    engine.playerPlayCard("Ollo", cardidOllo);
    engine.playerPlayCard("Zambo", cardidZambo);
    engine.playerPlayCard("Martin", cardidMartin);

    engine.goToGuessPhase();

    try {
      engine.makeGuess("Ollo", cardidMartin);
      engine.makeGuess("Zambo", cardidOllo);
      engine.makeGuess("Martin", cardidOllo);
    } catch (error) {
      try {
        engine.makeGuess("Zambo", cardidOllo);
        engine.makeGuess("Martin", cardidOllo);
      } catch (error) {}
      try {
        engine.makeGuess("Martin", cardidOllo);
      } catch (error) {}
    }
    engine.calculatePoints();
    engine.newNarrator();

    const totPoints = engine
      .getState()
      .players.reduce((acc, ply) => acc + ply.points, 0);
    expect(totPoints).to.be.greaterThan(0);
  });
});
