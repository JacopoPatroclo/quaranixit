const { generateRandomId } = require("./factories");
const {
  GameAlreadyStarted,
  TooMuchPlayersError,
  NotAllPlayersHavePlayed,
  GameNotStarted,
  PlayerDoesNotHaveCardInHand,
  PlayerHaveAlreadyPlayed,
  TheCardIsNotOnTheBoard,
  NotAllPlayerHaveGuessed,
  GameNotInGuessingMode,
  NarratorCanNotGuess,
  PlayerNotFound,
  NotEnoughPlayes,
  CanNotJoinWhileNotPending,
  CanNotVoteIsOwnCard,
} = require("./errors");

const GAME_STATES = require("gamestates");

function hasGuessedRight(player, gameState) {
  const narratorCard = gameState.board.cards.find(
    (spot) => spot.pl_name === gameState.narrator
  ).card;
  return gameState.guesses[player.name]
    ? gameState.guesses[player.name].id === narratorCard.id
    : false;
}

function gavePointIfTheyGetIt(numberOfPoints, gameState) {
  return (player) => {
    if (hasGuessedRight(player, gameState)) {
      return {
        ...player,
        points: player.points + numberOfPoints,
      };
    } else {
      return player;
    }
  };
}

function shuffleUp(array) {
  const cloned = [...array];
  const pivot = [];

  while (cloned.length > 0) {
    const index = Math.random() * cloned.length;
    const ent = cloned.splice(index, 1)[0];
    pivot.push(ent);
  }

  return pivot;
}

function getWinner(gamestates) {
  const winners = gamestates.players
    .filter((ply) => ply.points >= 30)
    .sort((player1, player2) => {
      if (player1.points > player2.points) {
        return -1;
      }
      if (player1.points < player2.points) {
        return 1;
      }
      return 0;
    });
  const winner = winners.length > 0 ? winners[0] : null;
  return winner;
}

module.exports = (maxPlayers, deck, onChangeState = (a) => a) => {
  const gameState = {
    id: generateRandomId(),
    players: [],
    board: {
      visible: false,
      cards: [],
    },
    guesses: {},
    narrator: null,
    winner: null,
    aviablePlayers: maxPlayers,
    state: GAME_STATES.PENDING,
  };

  return {
    GAME_STATES,
    getId: () => gameState.id,
    getState: () => {
      return { ...gameState };
    },

    /**
     * Add a new player to the current game
     * @function
     * @param {Player} player - The object rapresentation of a player.
     */
    addPlayer: (player) => {
      if (gameState.players.length < maxPlayers) {
        const resumePrevPlayer = gameState.players.find(
          (ply) => ply.name === player.name
        );
        if (!resumePrevPlayer && gameState.state !== GAME_STATES.PENDING) {
          throw new CanNotJoinWhileNotPending();
        }
        if (resumePrevPlayer) {
          player.hand = resumePrevPlayer.hand;
          player.points = resumePrevPlayer.points;
        }

        gameState.players = [
          ...gameState.players.filter((ply) => ply.name !== player.name),
          player,
        ];
        onChangeState(gameState);
      } else {
        throw new TooMuchPlayersError();
      }
    },

    /**
     * Remove a player from the current game
     * @function
     * @param {Player} player - The object rapresentation of a player.
     */
    removePlayer: (player) => {
      const currentPlayer = gameState.players.find((pl) => pl.id === player.id);
      if (currentPlayer) {
        gameState.players = gameState.players.filter(
          (pl) => pl.id !== currentPlayer.id
        );
        onChangeState(gameState);
      } else {
        throw new PlayerNotFound();
      }
    },

    /**
     * Start a new game
     * @function
     */
    startGame: () => {
      if (gameState.state === GAME_STATES.PENDING) {
        if (gameState.players.length <= 2) {
          throw new NotEnoughPlayes();
        }
        gameState.board.cards = gameState.players.map((player) => ({
          pl_name: player.name,
        }));
        gameState.board.visible = false;
        gameState.players = gameState.players.map((player) => ({
          ...player,
          points: 0,
          hand: deck.draw(6),
        }));
        gameState.narrator =
          gameState.players[
            Math.floor(Math.random() * gameState.players.length)
          ].name;
        gameState.state = GAME_STATES.PLAYING;
        onChangeState(gameState);
      } else {
        throw new GameAlreadyStarted();
      }
    },

    /**
     * A user play a card on the board from his hand
     * @function
     * @param {string} pl_name - The player id.
     * @param {string} card - The card id.
     */
    playerPlayCard: (pl_name, c_id) => {
      if (gameState.state === GAME_STATES.PLAYING) {
        const card = deck.get(c_id);
        const playerHasCardInHand = !!gameState.players.find(
          (player) =>
            player.name === pl_name &&
            !!player.hand.find((somecard) => somecard.id === c_id)
        );

        const playerHadAlreadyPlay = !!gameState.board.cards.find(
          (boardSpot) => boardSpot.pl_name === pl_name && boardSpot.card
        );
        if (playerHasCardInHand && !playerHadAlreadyPlay) {
          gameState.players = gameState.players.map((player) => {
            if (player.name === pl_name) {
              return {
                ...player,
                hand: player.hand.filter((card) => card.id !== c_id),
              };
            } else {
              return player;
            }
          });
          gameState.board.cards = [
            ...gameState.board.cards.filter(
              (boardSpot) => boardSpot.pl_name !== pl_name
            ),
            {
              card,
              pl_name,
            },
          ];
          onChangeState(gameState);
        } else {
          throw playerHasCardInHand
            ? new PlayerHaveAlreadyPlayed()
            : new PlayerDoesNotHaveCardInHand(card);
        }
      } else {
        throw new GameNotStarted();
      }
    },

    /**
     * Move the game to the guess phase
     * @function
     */
    goToGuessPhase: () => {
      if (gameState.state === GAME_STATES.PLAYING) {
        if (gameState.board.cards.length === gameState.players.length) {
          gameState.board.cards = shuffleUp(gameState.board.cards);
          gameState.state = GAME_STATES.GUESSING;
          gameState.board.visible = true;
          onChangeState(gameState);
        } else {
          throw new NotAllPlayersHavePlayed();
        }
      } else {
        throw new GameNotStarted();
      }
    },

    /**
     * The user make a guess
     * @function
     * @param {string} pl_name - The player id.
     * @param {string} c_id - The card id.
     */
    makeGuess: (pl_name, c_id) => {
      const card = deck.get(c_id);
      if (gameState.narrator === pl_name) {
        throw new NarratorCanNotGuess();
      }
      if (gameState.state === GAME_STATES.GUESSING) {
        const isCardOnTheBoard = !!gameState.board.cards.find(
          (spot) => card.id === spot.card.id
        );
        const isPlayerOwnCard = !!gameState.board.cards.find(
          (spot) => card.id === spot.card.id && spot.pl_name === pl_name
        );
        if (isCardOnTheBoard) {
          if (isPlayerOwnCard) {
            throw new CanNotVoteIsOwnCard();
          }

          gameState.guesses[pl_name] = card;
          onChangeState(gameState);
        } else {
          throw new TheCardIsNotOnTheBoard();
        }
      } else {
        throw new GameNotInGuessingMode();
      }
    },

    /**
     * Select a new narrator
     * @function
     */
    newNarrator: () => {
      if (gameState.state === GAME_STATES.SHOWING) {
        const winner = getWinner(gameState);
        if (winner) {
          gameState.winner = winner;
          gameState.state = GAME_STATES.WINNING;
          onChangeState(gameState);
          return;
        }
        const numberOfPlayers = gameState.players.length;
        const indexOfNarrator = gameState.players.indexOf(
          (player) => player.name === gameState.narrator
        );
        gameState.narrator =
          gameState.players[
            // Restart the turn if all players have been the narrator
            indexOfNarrator >= numberOfPlayers - 1 ? 0 : indexOfNarrator + 1
          ].name;
        deck.discard(gameState.board.cards.map((boardSpot) => boardSpot.card));
        gameState.board.cards = gameState.players.map((player) => ({
          pl_name: player.name,
        }));
        gameState.board.visible = false;
        gameState.players = gameState.players.map((player) => ({
          ...player,
          hand: [...player.hand, deck.draw(1)],
        }));
        gameState.guesses = {};
        gameState.state = GAME_STATES.PLAYING;
        onChangeState(gameState);
      } else {
        throw new GameNotStarted();
      }
    },

    /**
     * Calculate and give de points
     * @function
     */
    calculatePoints: () => {
      if (gameState.state === GAME_STATES.GUESSING) {
        const listPlayers = () =>
          gameState.players.filter(
            (player) => player.name !== gameState.narrator
          );
        const narrator = () =>
          gameState.players.find(
            (player) => player.name === gameState.narrator
          );
        const numberOfPlayers = gameState.players.length;
        const numberOfPlayersWithoutNarrator = numberOfPlayers - 1;
        const allPlayerMinusNarratorHaveGuess =
          Object.keys(gameState.guesses).length ===
          numberOfPlayersWithoutNarrator;
        const numberOfRightPlayers = listPlayers().filter((player) =>
          hasGuessedRight(player, gameState)
        ).length;

        const calculatePointsHandler = () => {
          // Everyone got it or no one +2 to all no narrator
          if (
            numberOfPlayersWithoutNarrator === numberOfRightPlayers ||
            numberOfRightPlayers === 0
          ) {
            gameState.players = [
              ...listPlayers().map((player) => ({
                ...player,
                points: player.points + 2,
              })),
              narrator(),
            ];
            return;
          }

          const narratorObject = narrator();
          gameState.players = [
            ...listPlayers().map(gavePointIfTheyGetIt(3, gameState)),
            {
              ...narratorObject,
              points:
                narratorObject.points + (numberOfRightPlayers > 0 ? 3 : 0),
            },
          ];

          // Give points to players when their card has been guessed
          gameState.players = [
            ...listPlayers().map((player) => {
              const spotPlayed = gameState.board.cards.find(
                (spot) => spot.pl_name === player.name
              );
              const timesSomeOneGuessIt = Object.keys(gameState.guesses)
                .map((key) => gameState.guesses[key])
                .filter((guessedCard) =>
                  spotPlayed ? spotPlayed.card.id === guessedCard.id : false
                ).length;
              return {
                ...player,
                points: player.points + timesSomeOneGuessIt,
              };
            }),
            narrator(),
          ];
        };

        if (allPlayerMinusNarratorHaveGuess) {
          calculatePointsHandler();
          gameState.state = GAME_STATES.SHOWING;
          onChangeState(gameState);
        } else {
          throw new NotAllPlayerHaveGuessed();
        }
      } else {
        throw new GameNotInGuessingMode();
      }
    },
  };
};
