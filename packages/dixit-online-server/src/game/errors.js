class TooMuchPlayersError extends Error {
  constructor() {
    super("Maximum number of player for this game has been reached");
  }
}

class GameAlreadyStarted extends Error {
  constructor() {
    super("The game is already started");
  }
}

class NotAllPlayersHavePlayed extends Error {
  constructor() {
    super("Not all the player have played their card");
  }
}

class GameNotStarted extends Error {
  constructor() {
    super("You silly, you cant't play a card now...");
  }
}

class PlayerDoesNotHaveCardInHand extends Error {
  constructor(card) {
    super(`The player does not have the card ${card.id} in hand`);
  }
}

class PlayerHaveAlreadyPlayed extends Error {
  constructor() {
    super("The player has already played");
  }
}

class TheCardIsNotOnTheBoard extends Error {
  constructor() {
    super("The selected card is not on the board");
  }
}

class NotAllPlayerHaveGuessed extends Error {
  constructor() {
    super(
      "Not all the player have guessed, whait when all the player have take a guess"
    );
  }
}

class GameNotInGuessingMode extends Error {
  constructor() {
    super(
      "The player can't take a guess before all the players have played theri card"
    );
  }
}

class NarratorCanNotGuess extends Error {
  constructor() {
    super("Smarty pants, if you are the narrator, you can't vote");
  }
}

class PlayerNotFound extends Error {
  constructor() {
    super("You are not currently in this game");
  }
}

class NotEnoughPlayes extends Error {
  constructor() {
    super("There are not enough player to start the game");
  }
}

class CanNotJoinWhileNotPending extends Error {
  constructor() {
    super(
      "You can't yoin this game because it is alredy started without you... sad..."
    );
  }
}

class CanNotVoteIsOwnCard extends Error {
  constructor() {
    super("You can't vote your own card!! You silly...");
  }
}

module.exports = {
  GameAlreadyStarted,
  TooMuchPlayersError,
  NotAllPlayersHavePlayed,
  GameNotStarted,
  PlayerDoesNotHaveCardInHand,
  PlayerHaveAlreadyPlayed,
  TheCardIsNotOnTheBoard,
  NotAllPlayerHaveGuessed,
  GameNotInGuessingMode,
  NarratorCanNotGuess,
  PlayerNotFound,
  NotEnoughPlayes,
  CanNotJoinWhileNotPending,
  CanNotVoteIsOwnCard,
};
