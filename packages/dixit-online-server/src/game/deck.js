class CardNotFound extends Error {
  constructor() {
    super("Smarty pants, if you are the narrator, you can't vote");
  }
}

module.exports = (withCards) => {
  let currentDeck = [...withCards];
  let discardPile = [];

  const popRandom = () => {
    const cardRandomlySelect =
      currentDeck[Math.floor(Math.random() * currentDeck.length)];
    currentDeck = currentDeck.filter(({ id }) => cardRandomlySelect.id !== id);
    return cardRandomlySelect;
  };

  return {
    count: () => currentDeck.length,
    get: (cardId) => {
      const card = withCards.find(({ id }) => id === cardId);
      if (!card) {
        throw new CardNotFound();
      }
      return card;
    },
    draw: (numberOfCards) => {
      if (numberOfCards > currentDeck.length) {
        currentDeck = [...discardPile];
      }
      let counter = numberOfCards;
      const store = [];
      store.push(popRandom());
      while ((counter -= 1)) {
        store.push(popRandom());
      }
      return numberOfCards === 1 ? store[0] : store;
    },
    discard: (cardOrCards) => {
      if (Array.isArray(cardOrCards)) {
        discardPile = [...discardPile, ...cardOrCards];
      } else {
        discardPile.push(cardOrCards);
      }
    },
  };
};
