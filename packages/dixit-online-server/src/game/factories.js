const generateRandomId = () => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1)
    .toUpperCase();
};

module.exports = {
  generateRandomId,
  /**
   * @typedef Player
   * @property {string} name - The player name.
   * Create a player
   * @function
   * @param {string} name - The player name.
   * @returns {Player} - The object rapresentation of a player.
   */
  makePlayer: (name, id) => ({
    id: id ? id : generateRandomId(),
    name,
  }),

  /**
   * @typedef Card
   * @property {string} id - The card id.
   * @property {string} link - The card resource image.
   * Create a card
   * @function
   * @param {string} id - The card name.
   * @returns {Card} - The object rapresentation for a card.
   */
  makeCardFromPublic: (id) => ({
    id,
    link: `/cards/${id}.jpg`,
  }),
};
