const { expect } = require("chai");
const MakeDeck = require("./deck");

const cards = [
  {
    id: "a",
    link: "/a.jpg"
  },
  {
    id: "b",
    link: "/b.jpg"
  },
  {
    id: "c",
    link: "/c.jpg"
  },
  {
    id: "d",
    link: "/d.jpg"
  },
  {
    id: "e",
    link: "/e.jpg"
  },
  {
    id: "f",
    link: "/f.jpg"
  },
  {
    id: "g",
    link: "/g.jpg"
  }
];

describe("Test the deck", () => {
  it("Shoud draw two cards", () => {
    const deck = MakeDeck(cards);
    const crds = deck.draw(2);
    expect(crds.length).to.be.eq(2);
    expect(deck.count()).to.be.eq(cards.length - 2);
  });
  it("Shoud use the discard pile if you ovredraw", () => {
    const deck = MakeDeck(cards);
    const crds = deck.draw(6);
    deck.discard(crds);
    const newcards = deck.draw(4);
    expect(newcards.length).to.be.eq(4);
  });
});
