const createIo = require("socket.io");
const actions = require("actions");
const makeGameEngine = require("./game");
const makeDeck = require("./game/deck");
const { makePlayer, generateRandomId } = require("./game/factories");
const extractCards = require("./cardExtractor");
const { join } = require("path");

class NoGameFoundError extends Error {
  constructor() {
    super("No game with that id is found");
  }
}

class ValidationError extends Error {
  constructor(key, value) {
    super(`The field ${key} with the value ${value} is not valid`);
  }
}

const makeHandleEventWrapper = (socket) => (action, fn) => {
  socket.on(action, (...arguments) => {
    try {
      console.log(
        `Try Action=${action}, id=${socket.id}, message=${JSON.stringify(
          arguments
        )}`
      );
      fn(...arguments);
      console.log(`Done Action=${action}, id=${socket.id}`);
    } catch (error) {
      socket.emit(actions.GAME_ERROR, {
        message: error.message,
        id: generateRandomId(),
      });
      console.log(`Fail Action=${action}, id=${socket.id}`);
      console.log(error);
    }
  });
};

const currentGames = [];

let CARDS;

extractCards(
  process.env.NODE_ENV !== "prod"
    ? join(__dirname, "..", "..", "dixit-online-client", "public", "cards")
    : join(__dirname, "..", "public", "cards")
)
  .then((cards) => {
    CARDS = cards;
  })
  .catch((err) => {
    throw err;
  });

module.exports = {
  attach: (http) => {
    const io = createIo(http, { path: "/ws", pingTimeout: 60000 });

    io.on("connection", (socket) => {
      console.log(`User connected, id=${socket.id}`);

      const handle = makeHandleEventWrapper(socket);

      handle(actions.NEW_GAME, (msg) => {
        if (!msg.name) {
          throw new ValidationError("username", msg.name);
        }
        const newGame = makeGameEngine(12, makeDeck(CARDS), (newGameState) => {
          io.to(newGameState.id).emit(actions.UPDATE_GAME_STATE, newGameState);
        });
        currentGames.push(newGame);
        const player = makePlayer(msg.name, socket.id);
        socket.join(newGame.getId());
        try {
          newGame.addPlayer(player);
          socket.emit(actions.IS_THE_CURRENT_PLAYER, player.id);
        } catch (error) {
          socket.leave(newGame.getId());
          throw error;
        }
      });

      handle(actions.JOIN_GAME, (msg) => {
        if (!msg.gameId) {
          throw new ValidationError("game ID", msg.gameId);
        }
        if (!msg.name) {
          throw new ValidationError("username", msg.name);
        }
        const game = currentGames.find((game) => game.getId() === msg.gameId);
        if (game) {
          socket.join(game.getId());
          const player = makePlayer(msg.name, socket.id);
          try {
            game.addPlayer(player);
            socket.emit(actions.IS_THE_CURRENT_PLAYER, player.id);
          } catch (error) {
            socket.leave(game.getId());
            throw error;
          }
        } else {
          throw new NoGameFoundError();
        }
      });

      handle(actions.LEAVE_GAME, (msg) => {
        if (!msg.gameId) {
          throw new ValidationError("game ID", msg.gameId);
        }
        if (!msg.playerId) {
          throw new ValidationError("player id", msg.playerId);
        }
        const game = currentGames.find((game) => game.getId() === msg.gameId);
        if (game) {
          game.removePlayer(makePlayer("none", msg.playerId));
          if (game.getState().players.length === 0) {
            // Remove the room
            delete currentGames[game.id];
            io.to(game.getState().id).emit(actions.UPDATE_GAME_STATE, null);
            socket.leave(game.getState().id);
          } else {
            // Remove the socket from the room
            socket.leave(game.getState().id);
            // Reset the game state
            socket.emit(actions.UPDATE_GAME_STATE, null);
          }
        } else {
          throw new NoGameFoundError();
        }
      });

      handle(actions.START_PLAING, (msg) => {
        if (!msg.gameId) {
          throw new ValidationError("game ID", msg.gameId);
        }
        const game = currentGames.find((game) => game.getId() === msg.gameId);
        if (game) {
          game.startGame();
        } else {
          throw new NoGameFoundError();
        }
      });

      handle(actions.PLAY_CARD, (msg) => {
        if (!msg.gameId) {
          throw new ValidationError("game ID", msg.gameId);
        }
        if (!msg.playerName) {
          throw new ValidationError("player name", msg.playerName);
        }
        if (!msg.cardId) {
          throw new ValidationError("card ID", msg.cardId);
        }
        const game = currentGames.find((gm) => gm.getId() === msg.gameId);
        if (game) {
          game.playerPlayCard(msg.playerName, msg.cardId);
          const allHavePlayed = game
            .getState()
            .board.cards.map((spot) => !!spot.card)
            .reduce((acc, hasPlayed) => acc && hasPlayed, true);
          if (allHavePlayed) {
            game.goToGuessPhase();
          }
        } else {
          throw new NoGameFoundError();
        }
      });

      handle(actions.VOTE_CARD, (msg) => {
        if (!msg.gameId) {
          throw new ValidationError("game ID", msg.gameId);
        }
        if (!msg.playerName) {
          throw new ValidationError("player name", msg.playerName);
        }
        if (!msg.cardId) {
          throw new ValidationError("card ID", msg.cardId);
        }
        const game = currentGames.find((gm) => gm.getId() === msg.gameId);
        if (game) {
          game.makeGuess(msg.playerName, msg.cardId);
          const gameState = game.getState();
          const allHaveGuessed =
            Object.keys(gameState.guesses).length ===
            gameState.players.length - 1;
          if (allHaveGuessed) {
            game.calculatePoints();
          }
        } else {
          throw new NoGameFoundError();
        }
      });

      handle(actions.NEXT_TURN, (msg) => {
        if (!msg.gameId) {
          throw new ValidationError("game ID", msg.gameId);
        }
        const game = currentGames.find((game) => game.getId() === msg.gameId);
        if (game) {
          game.newNarrator();
        } else {
          throw new NoGameFoundError();
        }
      });

      handle(actions.GET_GAME_STATE, (msg) => {
        if (!msg.gameId) {
          throw new ValidationError("game ID", msg.gameId);
        }
        const game = currentGames.find((game) => game.getId() === msg.gameId);
        if (game) {
          socket.join(game.getId());
          socket.emit(actions.UPDATE_GAME_STATE, game.getState());
        } else {
          throw new NoGameFoundError();
        }
      });

      // After register all the handler
      if (socket.handshake.query.gameId && socket.handshake.query.playerName) {
        let { gameId, playerName } = socket.handshake.query;
        console.log(
          `Reconnect player named ${playerName} to the game ${gameId}`
        );
        const game = currentGames.find((game) => game.getId() === gameId);
        if (game) {
          socket.join(game.getId());
          const player = makePlayer(playerName, socket.id);
          try {
            game.addPlayer(player);
            socket.emit(actions.IS_THE_CURRENT_PLAYER, player.id);
          } catch (error) {
            socket.leave(game.getId());
            throw error;
          }
        } else {
          socket.emit(actions.GAME_ERROR, {
            message: new NoGameFoundError().message,
            id: generateRandomId(),
          });
          // Reset the game state
          socket.emit(actions.UPDATE_GAME_STATE, null);
          console.log(`Reconnect failed no game aviable`);
        }
      }
    });
  },
};
